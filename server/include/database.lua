#!/usr/bin/env lua

--[[
University of Louisville USLI, 2011-2012

Filename                : database.lua
Written by              : Conor Heine
Date                    : 14 December 2011
Description             : Database interface module for managing wirelessly transmitted
                                  science data.
Dependencies            : MySQL
                          Luasql-mysql
]]

require 'luasql.mysql'

-- Connection vars
local DBNAME = "usli_dashboard"
local DBUSER = "dashboard"
local DBPASS = "uslid4$hboard"
local DBHOST = "localhost"
local DBPORT = "3306"

-- Class vars
local db         = {}
local launchId   = 0
local tableNames = {
        GPS     = "Gps_data",
        BAR     = "Bar_data",
        HUM     = "Hum_data",
        SIR     = "Sir_data",
        UV      = "Uv_data",
        RGB     = "Rgb_data",
        IMU     = "Imu_data"
}

-- Create the environment
local env = assert(luasql.mysql())
-- Establish the connection
local conn = assert(env:connect(DBNAME, DBUSER, DBPASS, DBHOST, DBPORT))

local function closeThis()
	conn:close()
	env:close()
end

-- close() : closes db connection
function db.close()
	closeThis()
end

-- launchIdExists() : Checks to see if the given id exists in the Launches table.
-- Returns true or false based on the status.
function db.launchIdExists(id)
        local q = string.format("SELECT LaunchID FROM Launches WHERE LaunchID = %d", id)
        local r = conn:execute(q)
        local row = r:fetch({},"a")
        
        if not(row and row.LaunchID) then
                return false
        else
                return true
        end
end

-- getData(launchId, sensorName, [lastUpdate]) : fetches data from DB for given launchId and sensor name.
-- an optional lastUpdate parameter will only select data with a timestamp > lastUpdate
function db.getData(launchId, sensorName, ...)
	local lastUpdate = 0
	if ... then 
		lastUpdate = ... 
	end

	local tableName = tableNames[sensorName]

	local q = string.format("SELECT * FROM %s WHERE FK_LaunchID = %d AND Timestamp > %d", tableName, launchId, lastUpdate)
	local cursor = assert(conn:execute(q))
	local row = cursor:fetch({},"a")

	local retTable = {}
	
	while row do
		table.insert(retTable, row)
		row = cursor:fetch({}, "a")
	end
	
	return retTable
end

function db.getLaunchName(launchId)
	local q = string.format("SELECT LaunchName FROM Launches WHERE LaunchID = %d", launchId)
	local cursor = assert(conn:execute(q))
	local row = cursor:fetch({},"a")

	return row.LaunchName
end

-- Return db table
return db
