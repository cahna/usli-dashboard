#!/usr/bin/env lua

require 'CodeGen'
local db = require 'include/database'
local graphs = {}
local seriesTemp = "{name:'?n',data:[?d]},"

local function dataPoint(y,x)
	return string.format('[%s,%s],',y,x)
end

-- Formats all sensor data from each science board and plots it in one graph with
-- multiple lines (DOES NOT INCLUDE AJAX LIVE UPDATE)
local function formatSeries(chart, data)
	-- Table that holds each different sensor's data points definition
        local sensors = {}

        -- For all database rows returned, add the data to the appropriate series
        for k,dataTab in pairs(data) do
                local sNum = dataTab.SensorNum

                -- Has the given sensor number been handled already?
                if sensors[sNum] then
                        -- If so, append the data to the current data list
                        sensors[sNum]['data'] = sensors[sNum]['data']..dataPoint(dataTab.Timestamp, dataTab[chart.yAxis])
                else
                        -- Otherwise, create an entry for the sensor number and add data
                        sensors[sNum] = {
                                series = seriesTemp,
                                data   = ""
                        }

                        -- Add the unique chart name (id'ed by sensor number)
                        sensors[sNum]['series'] = sensors[sNum]['series']:gsub('?n', chart.name..sNum)

                        -- Append the data point as x,y pair
                        sensors[sNum]['data'] = sensors[sNum]['data']..dataPoint(dataTab.Timestamp, dataTab[chart.yAxis])
                end
        end

	-- Now, we have a table of sensors and their associated data...
        -- For each sensor, remove trailing commas from each sensors data string,
        -- format the series, and add data to complete series
        local totalSeries = ""
        for sNum, tab in pairs(sensors) do
                tab['data']:sub(1, tab['data']:len() - 1) -- Remove trailing comma
                tab.series = tab.series:gsub("?d", tab['data']) -- Add data to series
                totalSeries = totalSeries..tab.series
        end

        -- Remove trailing comma from totalSeries
        totalSeries = totalSeries:sub(1, totalSeries:len() - 1)

	-- Return the new series
	return totalSeries
end

local function formatLiveSeries(chart, data)
	-- Add the most recent items to the series
	local mostRecentData = {}
	local dataCount      = 0
	local series         = seriesTemp
	local dataList       = ''
	
	-- Add series name
	series = series:gsub("?n", chart.name)

	-- Add data to series
	for k,dataTab in pairs(data) do
		dataList = dataList..dataPoint(dataTab.Timestamp, dataTab[chart.yAxis])
	end

	-- Remove trailing comma
	dataList = dataList:sub(1, dataList:len()-1)

	series = series:gsub("?d", dataList)

	return series
end

local function newChart(chart, live)
        -- Open template file
	local tFile = 'etc/templates/graph.tmpl'
	if live then
		tFile = 'etc/templates/liveGraph.tmpl'
	end

	local gTemp = assert(dofile(tFile))
        gTemp.chart = chart

        return gTemp('new')
end

graphs.gpsAltLive = function(_launchId)
	-- Define chart characteristics
	local chart = {
		name       = 'GPS',
		height     = 250,
		yAxis      = 'Altitude',
		seriesData = '',
		limitData  = true,
		maxDataPoints = 20,
		launchId   = _launchId
	}

	-- Get the sensor's data from the DB
	local data = db.getData(chart.launchId, chart.name, 0)

	-- Format the chart series data
	chart.seriesData = formatLiveSeries(chart, data)
	
	return newChart(chart, true)
end

graphs.hum = function(_launchId)
	-- Define chart characteristics
	local chart = {
		name       = 'HUM',
		height     = 250,
		yAxis      = 'Humidity',
		seriesData = '',
		launchId   = _launchId
	}

	-- Get the sensor's data from the DB
	local data = db.getData(chart.launchId, chart.name, 0)
	
	-- Format the chart series data
	chart.seriesData = formatLiveSeries(chart, data)

	-- Return the templated chart definition
	return newChart(chart)
end

graphs.bar = function(_launchId)
	-- Define chart characteristics
        local chart = {
                name       = 'BAR',
                height     = 250,
                yAxis      = 'Pressure',
                seriesData = '',
		launchId   = _launchId
        }

        -- Get the sensor's data from the DB
        local data = db.getData(chart.launchId, chart.name, 0)

        -- Format the chart series data
        chart.seriesData = formatSeries(chart, data)

        -- Return the templated chart definition
        return newChart(chart)
end

graphs.sir = function(_launchId)
        -- Define chart characteristics
        local chart = {
                name       = 'SIR',
                height     = 250,
                yAxis      = 'Irradiance',
                seriesData = '',
		launchId   = _launchId
        }

        -- Get the sensor's data from the DB
        local data = db.getData(chart.launchId, chart.name, 0)

        -- Format the chart series data
        chart.seriesData = formatSeries(chart, data)

        -- Return the templated chart definition
        return newChart(chart)
end

graphs.uv = function(_launchId)
        -- Define chart characteristics
        local chart = {
                name       = 'UV',
                height     = 250,
                yAxis      = 'Radiation',
                seriesData = '',
		launchId   = _launchId
        }

        -- Get the sensor's data from the DB
        local data = db.getData(chart.launchId, chart.name, 0)

        -- Format the chart series data
        chart.seriesData = formatSeries(chart, data)

        -- Return the templated chart definition
        return newChart(chart)
end


return graphs

