#!/usr/bin/env lua

local db = require 'include/database'
local updaters = {}

local function formatPairs(yAxis, data)
	local formatted = {}
	for k,dataTable in pairs(data) do
		table.insert(formatted, {dataTable.Timestamp * 1, dataTable[yAxis] * 1})
	end

	return formatted
end

local function gpsAltLive(launchId, lastUpdate)
	-- Define chart characteristics
	local chart = {
		yAxis      = 'Altitude',
		name       = 'GPS'
	}

	-- Get the sensor's data from the DB
	local data = db.getData(launchId, chart.name, lastUpdate)

	-- Format the chart series data
	local mostRecentDataPairs = formatPairs(chart.yAxis, data)
	
	return mostRecentDataPairs
end

local funcs = {
	GPS = gpsAltLive
}

updaters.getUpdatedData = function(launchId, sensorName, lastUpdate)
	return funcs[sensorName](launchId, lastUpdate)
end

return updaters

