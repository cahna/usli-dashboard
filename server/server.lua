#!/usr/bin/env lua
--[[
University of Louisville USLI, 2011-2012

Filename       : server.lua
Written by     : Conor Heine
Date           : 20 January 2012
Description    : Handle HTTP requests from the dashboard's GUI frontend 
Dependencies   : Wsapi (http://keplerproject.github.com/wsapi/) [GPL-Compatible]
                 Orbit (http://keplerproject.github.com/orbit/) [GPL-Compatible]
                 Xavante (http://keplerproject.github.com/xavante/) [GPL-Compatible]
                 lua-CodeGen (http://fperrad.github.com/lua-CodeGen/codegen.html) [MIT/X11]
]]

require 'orbit'           -- MVC web framework for lua
require 'logging.file'    -- Logging API
require 'CodeGen'         -- Templating engine
require 'json'

-- Data logging
local timestamp = os.date("%d.%m.%y_%X")
local logDir    = "etc/logs/"
local logName   = logDir.."server.log"
local logger    = logging.file(logName, "%X", "[%level] %message\n")

-- **************
-- * Web Server *
-- **************

-- Declare the module
module("server", package.seeall, orbit.new)


-- Web handler functions to be mapped to below
function index(web)	
	local graphs = require 'include/graphs'

	local render = {
		graphs.hum,
		graphs.bar,
		graphs.sir,
		graphs.uv,
		graphs.gpsAltLive
	}
	
	local page = assert(dofile('etc/templates/htmlPage.tmpl'))
	local db = require 'include/database'
	
	local launchId = 1

	if web.GET['launch'] then
		launchId = web.GET['launch'] * 1
	end

	local launchName = db.getLaunchName(launchId)

	local allGraphs = ""
	for k,gFunc in pairs(render) do
		allGraphs = allGraphs..gFunc(launchId)
	end	

	page.charts = allGraphs
	page.launchName = launchName
	return page('html')
end

function update(web)
	local updaters = require 'include/updaters'
	local db = require 'include/database'
	local g = web.GET
	local error = {failure = true}
	local sensorName, launchId, lastUpdate = g['chart'], g['launch']*1, g['last']*1

	if not (sensorName or launchId) then 
		return json.encode(error)
	else
		local ret = updaters.getUpdatedData(launchId, sensorName, lastUpdate)

		if not ret then
			return json.encode(error)
		else
			return json.encode(ret)
		end
	end
end

-- **************
-- * Dispatcher *
-- **************

-- Map (dispatch) the handler functions to specific URL's
server:dispatch_get(index, "/", "/index")
server:dispatch_get(update, "/update")

-- *********
-- * Setup *
-- *********

-- Test connections before starting web server
print("Server started")
