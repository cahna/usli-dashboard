/*
 * This file assumes that all dependencies are properly installed
 * and the user has proper administrative privileges with the
 * local OS and mysql server instance to execute the following
 * commands.
 * Written by: Conor Heine, 6 March 2012
 *
 * Usage: mysql -u root -p < createDatabase.sql
 */

/* Destructive part */
DROP DATABASE IF EXISTS usli_dashboard;
DROP USER 'dashboard'@'localhost';

/* Create the database */
CREATE DATABASE usli_dashboard;
USE usli_dashboard;

/* Create a user/password for the application to use */
CREATE USER 'dashboard'@'localhost' IDENTIFIED BY 'uslid4$hboard';
GRANT ALL ON usli_dashboard.* to 'dashboard'@'localhost';

/* Create the tables */
CREATE TABLE Launches (
  LaunchID    INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  LaunchName  VARCHAR(30) NOT NULL UNIQUE,
  Timestamp   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (LaunchID),
  INDEX(LaunchName)
) ENGINE=InnoDB;

CREATE TABLE Gps_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  LatDeg       FLOAT NOT NULL,
  LatMin       FLOAT NOT NULL,
  LatSec       FLOAT NOT NULL,
  LonDeg       FLOAT NOT NULL,
  LonMin       FLOAT NOT NULL,
  LonSec       FLOAT NOT NULL,
  Altitude     FLOAT NOT NULL,
  Speed        FLOAT NOT NULL,
  NumSats      INT(2) UNSIGNED NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE Bar_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  Pressure     FLOAT NOT NULL,
  Altitude     FLOAT NOT NULL,
  Temperature  FLOAT NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE Hum_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  Humidity     FLOAT NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE Sir_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  Irradiance   FLOAT NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE Uv_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  Radiation    FLOAT NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE Rgb_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  Red          FLOAT NOT NULL,
  Green        FLOAT NOT NULL,
  Blue         FLOAT NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE Imu_data (
  Timestamp    INT UNSIGNED NOT NULL,
  FK_LaunchID  INT(3) UNSIGNED NOT NULL,
  SensorNum    INT(2) UNSIGNED NOT NULL,
  X_Acc        FLOAT NOT NULL,
  Y_Acc        FLOAT NOT NULL,
  Z_Acc        FLOAT NOT NULL,
  Y_Rot        FLOAT NOT NULL,
  X_Rot        FLOAT NOT NULL,
  FOREIGN KEY (FK_LaunchID) REFERENCES Launches(LaunchID)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;
