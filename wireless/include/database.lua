#!/usr/bin/env lua

--[[
University of Louisville USLI, 2011-2012

Filename		: database.lua
Written by		: Conor Heine
Date			: 14 December 2011
Description		: Database interface module for managing wirelessly transmitted
				  science data.
Dependencies            : MySQL
                          Luasql-mysql
]]

require 'luasql.mysql'

-- Connection vars
local DBNAME = "usli_dashboard"
local DBUSER = "dashboard"
local DBPASS = "uslid4$hboard"
local DBHOST = "localhost"
local DBPORT = "3306"

-- Class vars
local database   = {}
local launchId   = 0
local launchName = ""
local tableNames   = {
	GPS     = "Gps_data",
	BAR     = "Bar_data",
	HUM     = "Hum_data",
	SIR     = "Sir_data",
	UV      = "Uv_data",
	RGB     = "Rgb_data",
	IMU     = "Imu_data"
}

-- Create the environment
local env = assert(luasql.mysql())
-- Establish the connection
local conn = assert(env:connect(DBNAME, DBUSER, DBPASS, DBHOST, DBPORT))

-- Private functions

-- createNewLaunch() : Inserts the new launch name into the Launches table and returns 
-- the auto-generated launch ID
local function createNewLaunch(name)
	-- Insert new launch name
	local insert = string.format("INSERT INTO Launches VALUES (null, '%s', null)", name)
	local res = conn:execute(insert)
	
	-- Get the LaunchID
	local fetch = "SELECT LAST_INSERT_ID() AS id"
	local cur = conn:execute(fetch)
	local row = cur:fetch({}, "a")

	-- Return the newly created id and name
	return row.id, name
end

-- launchIdExists() : Checks to see if the given id exists in the Launches table.
-- Returns true or false based on the status.
local function launchIdExists(id)
	local q = string.format("SELECT * FROM Launches WHERE LaunchID = %d", id)
	local r = conn:execute(q)
	local row = r:fetch({},"a")
	
	if not(row and row.LaunchID and row.LaunchName) then
		return false
	else
		return true, row.LaunchName
	end
end

local function formatData(sensorName, d)
	local str = ""

	if sensorName == "GPS" then
		str = string.format("%f, %f, %f, %f, %f, %f, %f, %f, %f", 
				d['LATD'], 
				d['LATM'], 
				d['LATS'], 
				d['LOND'], 
				d['LONM'], 
				d['LONS'], 
				d['ALT'], 
				d['SPD'], 
				d['NSAT'])
	elseif sensorName == "BAR" then
		str = string.format("%f, %f, %f", d['P'], d['A'], d['T'])
	elseif sensorName == "HUM" then
		str = string.format("%f", d['H'])
	elseif sensorName == "SIR" then
		str = string.format("%f", d['S'])
	elseif sensorName == "UV" then
		str = string.format("%f", d['R'])
	elseif sensorName == "IMU" then
		str = string.format("%f, %f, %f, %f, %f", d['XA'], d['YA'], d['ZA'], d['YR'], d['XR'])
	elseif sensorName == "RGB" then
		str = string.format("%f, %f, %f", d['R'], d['G'], d['B'])
	else
		str = ""
	end

	return str
end

-- Class functions

-- rows() : Returns an iterator function for the given sql statement
database.rows = function(connection, sql_statement)
        local cursor = assert (connection:execute(sql_statement))
        return function ()
                return cursor:fetch()
        end
end

-- insertData() : takes the valid dataTable and inserts its values accordingly
database.insertData = function(packet)
	local sensorName = packet['s']
	local tableName  = tableNames[sensorName]
	local timestamp  = packet['t']
	local sensorNum  = packet['n']
	local dataTable  = packet['d']

	local insertData = formatData(sensorName, dataTable)
	local q = string.format("INSERT INTO %s VALUES (%d, %d, %d, %s)", 
				tableName,
				timestamp,
				launchId,
				sensorNum,
				insertData)

	local r = conn:execute(q)
end

-- ***********************
-- *     Begin Setup     *
-- ***********************

-- Get the list of launches
local cursor = assert(conn:execute("SELECT LaunchID, LaunchName FROM Launches"))
local row = cursor:fetch({},"a")

-- Display Launch list
print("\n\tLaunch List")
print("\t**********")
print("\tID\tLaunch Name")
while row do
	print(string.format("\t%s\t%s",row.LaunchID, row.LaunchName))
	row = cursor:fetch(row, "a")
end
print()

-- Ask which launch to update, or start a new launch
io.write("Enter ID to update, or 0 to start new launch: ")
local input = io.read("*line")
if input == '0' then
	-- Ask for new launch name
	io.write("Enter new launch name: ")
	local lname = io.read("*line")
	launchId, launchName = createNewLaunch(lname)
else
	-- Check to see if the input was valid (a launch id that actually exists)
	local l_id = input * 1 -- convert to number

	local launchExists, _lName = launchIdExists(l_id)
	print(launchExists, _lName)
	if launchExists then
		launchId = l_id -- Set launch ID
		launchName = _lName -- Set launch name
	else
		-- Error... exit
		print("No launch exists for that ID")
		os.exit(1)
	end
end

-- Make launch vars public
database.launchId = launchId
database.launchName = launchName

print("Launch ID set to : "..launchId)

return database

