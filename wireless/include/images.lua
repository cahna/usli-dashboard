#!/usr/bin/env lua

local images = {}
local imgFolder  = "etc/images/%s/"
local launchName = ""

local function fileExists(name)
	local f = io.open(name, "r")

	if f ~= nil then 
		io.close(f) 
		return true 
	else 
		return false 
	end
end

images.isImage = function(packet)
	local regexCapture = "{\"t\":(%d+),\"s\":\"IMG\",\"n\":(%d+),\"l\":(%d+),\"d\":{\"JPG\":\"(.+)\"}}"
	local sensorName  = "IMG"
	local start, finish, timestamp, picNumber, length, data = packet:find(regexCapture)

	print(start, finish, timestamp)

	if not (start and finish) then
		return false, nil
	else
		-- Save vars in table and convert strings to numbers
		local dataTable = {
			t = timestamp*1,
			s = sensorName,
			n = picNumber*1,
			l = length*1,
			d = { jpg = data }
		}
		
		return true, dataTable
	end
end

-- saveImage() : Saves the image sent in the packet
images.saveImage = function(packet)
        local picnumber = packet['n']
        local dataTable = packet['d']
        local fileName  = imgFolder..picnumber..".jpg"
	local imgFile
	
	if fileExists(fileName) then
		-- The file already exists; append data
		imgFile = assert(io.open(fileName, 'a'))
	else
		-- Creating new file to begin writing
		imgFile   = assert(io.open(fileName, 'w'))
	end

        -- Write data to file
        imgFile:write(dataTable['JPG'])
        imgFile:close()
end

-- ***********************
-- *     Begin Setup     *
-- ***********************

images.setup = function(lName)
	launchName = lName

	-- Create image dir for launch (Doesn't matter if folder already exists)
	imgFolder = string.format(imgFolder, launchName)
	assert(os.execute("mkdir "..imgFolder))
end

return images
