#!/usr/bin/env lua

-- Define number of science boards used in the payload
local SCI_BOARDS = 8

local sensors = {}
local timeKey   = "t"
local sensorKey = "s"
local dataKey   = "d"
local numKey    = "n"
local failKey   = "fail"
local lengthKey = "l"

-- Import list of valid sensors and valid variables per sensor
sensors.validSensors = require 'include/validSensors'

-- Export key variables for external reference
sensors.timeKey   = timeKey
sensors.sensorKey = sensorKey
sensors.dataKey   = dataKey
sensors.numKey    = numKey
sensors.failKey   = failKey

--[[
  local sensorExists() :
    Determines if the passed sensor name exists in the
    config file. Returns true if found, and false otherwise.
]]
local function sensorExists(sensorName)
	-- Check to see if the sensor name exists 
	if sensors.validSensors[sensorName] then
		return true
	else
		return false
	end
end

--[[
  local varExists():
    Determines if the passed variable name exists for the given
    valid sensorName. Precondition: sensorName exists in validSensors
]]
local function varExists(sensorName, varName)
	-- If no sensorName is passed, or the sensorName doesnt exist
	if not sensorName or not sensors.validSensors[sensorName] then
		return false
	end

	-- If the varName for the sensorName exists, return true
	if sensors.validSensors[sensorName][varName] or varName == failKey then
		return true
	else
		return false
	end

end

--[[
  validateImagePacket():
    Determines if the passed image data table is valid
]]
local function validateImagePacket(dataTable)
	
	local imageNum   = dataTable[numKey]
	local sensorData = dataTable[dataKey]
	local imgDataLen

	-- Check for length key
	if not dataTable[lengthKey] then
		return false, "Image packet missing a length key"
	else
		imgDataLen = dataTable[lengthKey]
	end

	-- Type check length key
	if type(imgDataLen) ~= "number" then
		return false, "Image length not of type number."
	end

	-- Check to see if there's a JPG key in the data
	if not sensorData["JPG"] then
		return false, "Image missing JPG data"
	end

	-- Type check JPG data
	if type(sensorData["JPG"]) ~= "string" then
		return false, "Image JPG data not of type 'string'"
	end

	-- Check to see if the length of image data equals the value sent from the arduino
	if imageDataLen ~= string.len(sensorData["JPG"]) then
		return true, "Packet is valid, but the data lengths do not match"
	end

	return true, "Valid image packet"	
end

--[[
  validateDataPacket():
    Determines if the passed science data table is valid
]]
local function validateDataPacket(dataTable)
	local sensorName = dataTable[sensorKey]
        local sensorData = dataTable[dataKey]
	local sensorNum  = dataTable[numKey]
	
	-- Check to see if the sensor name is a valid entry
	if not sensorExists(sensorName) then

		-- The sensor is not predefined, return false
		return false, "Sensor name is not valid"

	elseif sensorNum >= SCI_BOARDS or type(sensorNum) ~= "number" then

		-- The sensor number is out of bounds (>= number of science boards) or invalid type
		return false, "Sensor number is either too large or invalid"

	else

		-- The sensor is defined, check for valid variables
		local validVars = true
		local message   = "Vars are valid"

		for varName,value in pairs(sensorData) do 

			if not varExists(sensorName, varName) then
				validVars = false
				message   = "Invalid variable, "..varName
				break
			else
				-- Variable was found, perform type checking
				varType = type(value)
				if varType ~= "number" then

					validVars = false
					message   = "Incorrect type for variable, "..varName..". Unexpected: "..varType
					break

				end
			end
		end

		-- Return status and message
		return validVars, message
	end
end

--[[
  sensors.validate() : 
    Determines if the passed data table contains a valid
    sensor name and variables in the correct format
]]
sensors.validate = function(dataTable)
        -- If a sensor name or data table were ommitted, then the data
        -- is not valid. Return false.  

        -- Check to see if data has all keys for a science sensor packet
        if not (dataTable[sensorKey] and dataTable[dataKey] and dataTable[timeKey] and dataTable[numKey]) then
                return false, "Missing packet key (check sensorKey, dataKey, numKey, and timeKey)"
        end

        local sensorName = dataTable[sensorKey]

        -- If the data is image data, then it needs a length key
        if sensorName == "IMG" then
                return validateImagePacket(dataTable)
        else
                return validateDataPacket(dataTable)
        end
end

return sensors
