#!/usr/bin/env lua

-- Testing file for the database library

print("BEGINNING TEST")

local db = require 'database'

print("\tTESTING INSERTS")

-- IMU Test data
local test1 = {
        t = 12345,
        s = "IMU",
        n = 4,
        d = {
                XA = 0.21,
                YA = 4.231,
                ZA = 722.131,
                YR = 0.2,
                XR = 0.5120
        }
}

-- GPS Test data
local test2 = {
        t = 4325123,
        s = "GPS",
        n = 0,
        d = {
                LATD = 81.12,
                LATM = 12.324,
                LATS = 0.2312,
                LOND = 121.04231,
                LONM = 32.431242,
                LONS = 0.123534,
                ALT = 732.4,
                SPD = 12.1,
                NSAT = 3
        }
}

-- BAR Test data
local test3 = {
	t = 98743123,
        s = "BAR",
        n = 7,
        d = {
		P = 0.241,
		A = 711.578294,
		T = 18.32401
	}
}

-- HUM Test data
local test4 = {
        t = 32432433,
        s = "HUM",
        n = 3,
        d = {
                H = 60.43241,
        }
}

-- SIR Test data
local test5 = {
        t = 111222333,
        s = "SIR",
        n = 6,
        d = {
                S = 4.46241,
        }
}

-- UV Test data
local test6 = {
        t = 32432433,
        s = "UV",
        n = 1,
        d = {
                R = 0.43241,
        }
}

-- RGB Test data
local test7 = {
	t = 5782382,
	s = "RGB",
	n = 3,
	d = {
		R = 0.213,
		G = 0.124,
		B = 0.432
	}
}

db.insertData(test1)
db.insertData(test2)
db.insertData(test3)
db.insertData(test4)
db.insertData(test5)
db.insertData(test6)
db.insertData(test7)
