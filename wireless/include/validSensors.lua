#!/usr/bin/env lua

local gps_vars = { -- GPS Module
	LATD = true, -- Degrees
	LATM = true, -- Minutes
	LATS = true, -- Seconds
	LOND = true, -- Degrees
	LONM = true, -- Minutes
	LONS = true, -- Seconds
	ALT  = true, -- Altitude
	SPD  = true, -- Speed
	NSAT = true  -- Number of satellite locks
}

local bar_vars = {
	P    = true, -- Barometeric Pressure
	A    = true, -- Altitude
	T    = true  -- Temperature
}

local hum_vars = {
	H    = true -- Relative humidity
}

local sir_vars = {
	S    = true -- Solar irradiance
}

local uv_vars = {
	R    = true -- UV radiation
}

local rgb_vars = {
	R    = true, -- Red
	G    = true, -- Green
	B    = true  -- Blue
}

local imu_vars = {
	XA       = true,
	YA       = true,
	ZA       = true,
	YR       = true,
	XR       = true
}

local img_vars = {
	JPG	= true
}

local actualSensors = {
	GPS	= gps_vars, 
	BAR	= bar_vars, 
	HUM	= hum_vars, 
	SIR     = sir_vars,
	UV	= uv_vars,
	RGB 	= rgb_vars, 
	IMU     = imu_vars, 
	IMG	= img_vars
}

return actualSensors
