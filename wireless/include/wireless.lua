#!/usr/bin/env lua
--[[
University of Louisville USLI, 2011-2012

Filename		: wireless.lua
Written by		: Conor Heine
Date			: 20 January 2012
Description		: Library to handle incoming wireless serial data
Dependencies            : ul_serial (http://luaforge.net/projects/ulserial/) [MIT/X]
                          lualogging (http://www.keplerproject.org/lualogging/) [GPL]
]]

require 'ul_serial'
require 'logging.file'

-- Serial-specific vars
local MIN_CHARS_PER_LINE = 10
local BAUD 		= 9600
local BITS 		= 8
local STOPS 	= 1
local PARITY 	= 'n'
local ports
local conn
local wireless	= {}

-- Data logging vars
local timestamp	= os.date("%d.%m.%y_%X")
local logDir	= "etc/logs/recv/"
local logName 	= logDir..timestamp..".log"
local logger	= logging.file(logName, "%X", "%message")

-- Data receive vars
local data = ""
local _charsReceived = 0

--[[
	wireless.open()
]]
wireless.open = function()
	-- Get ports list (result is saved in io.Serial.ports)
	io.Serial:getPorts()
	ports = io.Serial.ports
	
	-- If no serial ports found, keep trying?
	while not ports or not ports[1] do
		io.write("No serial ports found, try again? [y]: ")
		local input = io.read("*line")
		if input == "n" or input == "N" then
			os.exit(0)
		else
			io.Serial:getPorts()
			ports = io.Serial.ports
		end
	end
	
	-- Found USB/Serial ports, which to use? (Display list)
	print("\t*** Found ports: ***\n")
	for i,portName in ipairs(ports) do
		print("\t"..i.." : "..portName)
	end

	io.write("\nWhich port should be used?: ")
	local input = io.read("*line") + 0 -- +0 converts to number
	local PORT = ports[input]

	-- Open serial connection
	conn = io.Serial:open({
			port 	= PORT, 
			baud 	= BAUD, 
			bits 	= BITS, 
			stops 	= STOPS,
			parity	= PARITY
		})
end

wireless.close = function()
	conn:close()
end

--[[
	wireless.getLine() - Receives available serial data and attempts pattern
	matching to find a JSON packet. If a JSON packet is found, it is returned.
	Otherwise, nil is returned to indicate that either no data or not enough
	data was received.
]]
wireless.getNextPacket = function()
	local retval	

	if conn:availRX() < MIN_CHARS_PER_LINE then
		-- Not enough data ready yet
		retval = nil
	else
		-- Read the available data
		local mostRecentRead = conn:read()
		
		-- Log the read data and append it to the current data 'blob'
		logger:info(mostRecentRead)
		data = data..mostRecentRead
		_charsReceived = _charsReceived + string.len(mostRecentRead)		

		-- Use pattern matching to see if there is a JSON packet available
		local start, finish, jsondata = data:find("<([^<>\n]*)>")
		
		-- If JSON packet was found
		if start and finish and jsondata then
			-- Remove latest packet from available data string
			data = data:sub(finish + 1)
			
			-- Return the JSON string
			retval = jsondata
		else
			-- Flush RX and return nil (no recognizable packet received yet)			
		end
	end

	return retval
end

wireless.charsReceived = function()
	return _charsReceived
end

-- Open the serial connection
print("[  OK  ] Wireless.lua: Opening serial connection")
wireless.open()

-- Return available functions
return wireless

