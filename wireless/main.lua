#!/usr/bin/env lua

--[[
University of Louisville USLI, 2011-2012

Filename        : main.lua
Written by      : Conor Heine
Date            : 20 January 2012
Description     : Wireless receive, decode, and handle.
Dependencies    : include/serial.lua (Conor Heine)
                  include/database.lua (Conor Heine)
                  include/sensors.lua (Conor Heine)
]]

-- Dependencies

-- Requiring the wireless library will ask which /dev device to use and will
-- open the serial connection.
local wireless          = require 'include/wireless'

-- Requiring the database library will ask if a previous launch name is to be
-- updated or if a new launch is to be created
local database          = require 'include/database'
local images            = require 'include/images'
local sensors           = require 'include/sensors'
local json              = require 'json'
require 'logging.file'
require 'to_string'

-- Define how many times to iterate through the main loop before displaying stats
local REFRESH = 5000

-- Stat vars
local pReceived      = 0
local pDecodeSuccess = 0
local pDecodeFail    = 0
local pValid         = 0
local pInvalid       = 0
local iterations     = 0

-- Log vars
local errorLog = "etc/logs/error.log"
local logger   = logging.file(errorLog, "%X", "%message\n\n")

local function packetTotals()
	return "("..pReceived.." packets : "..pDecodeSuccess.." decoded / "..pDecodeFail.." failed : "..pValid.." valid / "..pInvalid.." invalid) ["..wireless.charsReceived().."]"
end

local function printStats(status)
	if iterations == 0 then
		if status then
			print("[   OK   ] "..packetTotals())
	        else
	                print("[  Error ] "..packetTotals())
	        end
	end	
end

local function incIterations()
	if iterations >= REFRESH then
		iterations = 0
	else
		iterations = iterations + 1
	end
end

-- Setup
images.setup(database.launchName)

-- Begin receive, decode, and handle data
while true do
	-- Get next available 'packet' of data
	local packet = wireless.getNextPacket()
	local valid, message

	-- If data was received, attempt json decode
	local status = true -- Indicates status of decode and validation of packet
	if packet then
		pReceived = pReceived + 1		
		
		-- Attempt to decode raw ascii into usable data table
		local success, dataTable = pcall(json.decode, packet)

		-- If initial decode fails, Check to see if it's an image packet with creepy characters
		if not success then
			success, dataTable = images.isImage(packet)
		end

		if not success then
			logger:warn("[ Decode Failure ] Packet text: "..packet)
			pDecodeFail = pDecodeFail + 1
			status = false
		else
			pDecodeSuccess = pDecodeSuccess + 1
			
			-- Validate packet
			valid, message = sensors.validate(dataTable)
			
			if valid then
				pValid = pValid + 1
				
				-- Handle case where data could be image
				if dataTable[sensors.sensorKey] == "IMG" then
					images.saveImage(dataTable)
				else
					database.insertData(dataTable)
				end
			else
				logger:warn("[ Invalid Packet ] ("..message..") Packet Text: "..packet)
				pInvalid = pInvalid + 1
				status = false
			end
		end
	end

	-- Print stats	
	printStats(status)
	incIterations()
end
