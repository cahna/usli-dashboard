#!/usr/bin/env lua

require 'json'
require 'to_string'

local testTab = {
	s = "sensorName",
	t = "timeStamp",
	d = {
		-- Sensor data
		exVal1 = 1.23,
		exval2 = -2.4
	}
}

local encoded = json.encode(testTab)
print(encoded)

print(string.len(encoded))

local decoded = json.decode(encoded)
print(To_String(decoded))
