#!/usr/bin/env lua

--[[
local rserial = assert(io.open("/dev/ttyACM0", "r"))
while true do
        chaine = rserial:read()
        rserial:flush()
        print(chaine)
end
]]
require 'to_string'
require 'ul_serial'
require 'json'

io.Serial:getPorts()
local ports = io.Serial.ports

while not ports or not ports[1] do
	io.write("No serial ports found, try again? [y]: ")
	local input = io.read("*line")
	if input == "n" or input == "N" then
		os.exit(0)
	else
		ports = io.Serial:getPorts()
	end
end

print("Found ports: "..To_String(ports))
io.write("Which port should be used?: ")
local input = io.read("*line") + 0 -- +0 converts to number

local prt = ports[input]
local p = io.Serial:open({port=prt, baud=9600,bits=8,stops=0,parity='n'})
local data = ""
local line = ""

while true do
	local avail = p:availRX()
	if avail > 25 then
		-- If sufficient data is available, read it
		data = p:read()
		--print(data)
		
		-- Flush serial
		p = p:flushRX()
		
		-- If the read data isn't null, append it to the line
		if data then
			line = line..data
			print(line)
		end
		
		-- Use pattern matching to see if there is a JSON packet available
		local start, finish, jsondata = line:find("<([^<>\n]*)>")

		if start and finish and jsondata then
			-- Display JSON string and decoded table
			print("Found data:"..jsondata)
			print("Decoded: "..To_String(json.decode(jsondata)))
			
			-- Remove latest packet from available data string
			line = line:sub(finish + 1)			
		end
	end
end

