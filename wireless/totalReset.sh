#!/bin/bash

IMGDIR="etc/images/"
SQLFILE="etc/createDatabase.sql"
PWD=`pwd`

# Remove all image dirs from etc/images
echo "Removing all image directories..."
cd $IMGDIR && ls | grep -v README | xargs rm -r 
cd $PWD

# Delete and recreate database
echo "Recreating database..."
mysql -u root -p < $SQLFILE
